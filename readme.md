# Python on Docker with JupyterLab,latex 

## Dir
### ./jpyter
jupyter files (.ipynb)

### ./src
python files (.py)

## settings
### requirement.txt
Write necessary package in ./docker/jupyterlab/requirements.txt   
Image includes 'pip3 install jupyterlab'

### jupyter
./docker/jupyterlab/jupyter_notebook_config.py

## Usage
docker-compose up -d

## etc
### virtualenv
If you don't need docker.    
You can use virtualenv.sh , It's make virtualenv    
```.env
sh ./virtualenv.sh
```
Then write necessary packages in ./requirements.txt

