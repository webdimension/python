import sys
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
print(sys.version)
# sns.set(font='IPAGothic')
# fig = plt.figure()
# ax = fig.add_subplot(1,1, 1)
# ticks = ax.set_xticks([0, 25, 50, 75, 100])
# labels = ax.set_xticklabels(['第1ステージ'
#                              '2nd Stage'
#                              '3nd Stage',
#                              'Last Stage',
#                              ''],
#                             rotation = 30, fontsize='small')
# ax.set_title('ステージ毎のポイント')
# ax.set_xlabel('ステージ')
# ax.set_ylabel('スコア')
# ax.plot(np.random.randint(-100, 100, size=100).cumsum())
# plot.savefig( 'plt.png' )
# cname = ['symboling', 'normalized-losses', 'make',
#          'fuel-type', 'aspiration', 'num-of-doors',
#          'body-style', 'drive-wheels', 'engine-location',
#          'wheel-base', 'length', 'width', 'height',
#          'curb-weight', 'engine-type',
#          'num-of-cylinders', 'engine-size', 'fuel-system',
#          'bore', 'stroke', 'compression-ratio',
#          'horsepower', 'peak-rpm', 'city-mpg',
#          'highway-mpg', 'price']
#
# df_data = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/autos/imports-85.data',
#                       header=None, names=cname, index_col=False)

# df_data = df_data.replace('?', np.nan)
# df_data = df_data.dropna()
# df_data['price'] = df_data['price'].astype(np.int64)
# print(df_data['price'].dtype)
# print(df_data['price'].dtype)
# df_data.head()
# print(df_data)
# print('shape:',df_data.shape)
# print('columns:',df_data.columns)
# print(df_data[['make', 'price']])
# print(df_data[['make', 'price']].loc[200:205])
# print(df_data[['make', 'price']].query('price > 30000'))
# print(df_data.groupby('make').max()[['price']])

