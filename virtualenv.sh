#!/usr/bin/env bash
python_version='3.7'
virtualenv_name='venv'
virtualenv -p python$python_version $virtualenv_name
source $virtualenv_name/bin/activate
pip3 install --upgrade pip
pip3 install jupyterlab
pip3 install -r requirements.txt
deactivate